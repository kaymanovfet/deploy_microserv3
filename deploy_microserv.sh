#!/bin/bash

echo --- Step 1. Getting repository
git clone https://github.com/microservices-demo/microservices-demo
cd microservices-demo

echo --- Step 2. Creating deployment
kubectl create -f deploy/kubernetes/manifests
nohup kubectl port-forward --address 0.0.0.0 services/front-end 30001:80 -n sock-shop & netstat -tulpn
