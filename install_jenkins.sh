#!/bin/bash

# Устанавливаем OpenJDK
echo --Step 5 install openjdk--
sudo apt-get install -y default-jdk

# Устанавливаем wget and ufw
echo --Step 6 install wget, ufw, sshpass
sudo apt-get install -y wget
sudo apt-get install -y ufw
sudo apt-get install -y sshpass

# Устанавливаем и запускаем Jenkins
echo --Step 7 install jenkins--
sudo wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo apt-key add -
sudo sh -c 'echo deb https://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'
sudo apt-get update
sudo apt-get install -y jenkins
sudo systemctl start jenkins
sudo service jenkins status
sudo ufw allow OpenSSH
sudo ufw allow 8080
sudo ufw status
